package edu.ntust.apvs.crawler.util;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author Carl Adler (C.A.)
 * */
public class APKFileHandlerTest {
	
	@Before
	public void before() {
	}
	
	@Test
	@Ignore
	public void testForInterateTheCrawlListFile() {
		List<String> result = APKFileHandler.getListFromFile(APVSConstantDescriptor.LIST_FOR_CRAWL);
		result.forEach(s -> System.out.println(s));
		assertEquals(3, result.size());
	}
	
	@Test
	public void testForNullList() {
		List<String> result = APKFileHandler.getListFromFile(APVSConstantDescriptor.LIST_FOR_CRAWL);
		assertEquals(0, result.size());
	}

}
