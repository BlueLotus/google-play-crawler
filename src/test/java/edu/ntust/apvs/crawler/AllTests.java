package edu.ntust.apvs.crawler;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import edu.ntust.apvs.crawler.dao.impl.CrawlerTaskDaoTest;
import edu.ntust.apvs.crawler.util.APKFileHandlerTest;

@RunWith(Suite.class)
@SuiteClasses({CrawlerTaskDaoTest.class, APKFileHandlerTest.class})
public class AllTests {

}
