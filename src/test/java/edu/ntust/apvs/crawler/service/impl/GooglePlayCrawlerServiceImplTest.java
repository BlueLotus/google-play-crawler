package edu.ntust.apvs.crawler.service.impl;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.support.GenericXmlApplicationContext;

import edu.ntust.apvs.crawler.dao.CrawlerTaskDao;
import edu.ntust.apvs.crawler.domain.APKManifest;
import edu.ntust.apvs.crawler.service.GooglePlayCrawlerService;
import edu.ntust.apvs.crawler.util.APVSConstantDescriptor;
import edu.ntust.apvs.crawler.util.Order;
import edu.ntust.apvs.crawler.util.OrderedRunner;

/**
 * @author Carl Adler (C.A.)
 * */
@RunWith(OrderedRunner.class)
public class GooglePlayCrawlerServiceImplTest {
	
	private static GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
	private static GooglePlayCrawlerService crawlerService;
	private static CrawlerTaskDao crawlerDao;
	
	@BeforeClass
	public static void before() {
		ctx.load("classpath:app-context-test.xml");
		ctx.refresh();
		crawlerService = ctx.getBean("crawlerService", GooglePlayCrawlerService.class);
		crawlerDao = ctx.getBean("crawlerTaskDao", CrawlerTaskDao.class);
	}
	
	@AfterClass
	public static void after() {
		ctx.close();
	}

	@Test
	@Order(order = 1)
	public void testForDownloadNewAPKFile() {
		crawlerService.crawlAPKFilesWithCustomList();
		assertEquals(1, crawlerDao.findByAPKFileName("com.evernote.apk").size());
		assertFalse(new File(APVSConstantDescriptor.APK_FILE_LOCATION + "com.evernote.apk").exists());
	}
	
	@Test
	@Order(order = 2)
	public void testForUpdateAPKFile() {
		APKManifest apkManifest = crawlerDao.findByAPKFileName("com.evernote.apk").get(0);
		apkManifest.setVersionCode("12345");
		crawlerDao.updateAPKMetaInfo(apkManifest);
		crawlerService.crawlAPKFilesWithCustomList();
		assertFalse(crawlerDao.checkVersionCodeEquality("com.evernote.apk", "12345"));
	}

}
