package edu.ntust.apvs.crawler.domain;

import java.io.Serializable;

/**
 * @author Carl Adler (C.A.)
 * */
public class APKDownloadInfo implements Serializable {
	
	private static final long serialVersionUID = -5022869988858982401L;
	
	private String packageName;
	private int versionCode;
	private int offerType;
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	public int getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}
	
	public int getOfferType() {
		return offerType;
	}
	public void setOfferType(int offerType) {
		this.offerType = offerType;
	}
	
	@Override
	public String toString() {
		return String.format("APK download info:\n"
				+ "  package name: %s\n"
				+ "  version code: %s\n"
				+ "  offer type: %s\n", packageName, versionCode, offerType);
	}
}
